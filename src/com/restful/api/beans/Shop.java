package com.restful.api.beans;

public class Shop implements Comparable<Shop> {
	private int id;
	private float price;
	private int stock;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public int compareTo(Shop shop) {
		float compareTo = price - shop.getPrice();
		if (compareTo == 0) {
			if (!equals(shop)) {
				return -1;
			}
		}

		return (int) compareTo;
	}
}
