package com.restful.api.beans;

import java.util.List;

public class Product {

	private String mpn;
	private int id;
	private List<Shop> array;

	public Product(String mpn, int id, List<Shop> shopsList) {
		this.mpn = mpn;
		this.id = id;
		this.array = shopsList;
	}

	public Product() {
	}

	public String getMpn() {
		return mpn;
	}

	public void setMpn(String mpn) {
		this.mpn = mpn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Shop> getArray() {
		return array;
	}

	public void setArray(List<Shop> array) {
		this.array = array;
	}

}
