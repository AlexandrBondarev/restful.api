package com.restful.api.beans;

import java.util.Comparator;

public class ShopDecreaseComparator implements Comparator<Shop> {
	
	
	public int compare(Shop one, Shop other) {
		return (int) (-(one.getPrice() - other.getPrice()));
	}

}
