package com.restful.api.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.restful.api.beans.Product;
import com.restful.api.managers.ProductManager;

@Controller
public class RestfulController {
	
	

	@Autowired
	private ProductManager productManager;

	private static List<Integer> CORECT_REQUEST;

	@RequestMapping(value = "/getprice", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	Object getProduct(
			@RequestParam(value = "mpn", required = false) String mpn,
			@RequestParam(value = "avalability", required = false) String avalabilityText,
			@RequestParam(value = "pricesort", required = false) String pricesortText,
			HttpServletResponse response) throws IOException {
		
		//������������� ��
		productManager.initializeBD();
		

		if ((mpn == null) || (mpn.length() < 1)) {
			return " ERROR \"mpn\" must be posted";
		}
		Product serviceSays;
		boolean isPostedSort = isPostedParametr(pricesortText);
		boolean isPostedAvalability = isPostedParametr(avalabilityText);
		if (!isPostedAvalability && !isPostedSort) {
			serviceSays = productManager.getProductByMPN(mpn);
			if (serviceSays == null) {
				return " ERROR  this \"mpn\" = " + mpn + " is not exist";
			} else {
				return serviceSays;
			}
		}
		int avalability = 0;
		if (isPostedAvalability) {
			try {
				avalability = Integer.valueOf(avalabilityText);
			} catch (NumberFormatException e) {
				return "ERROR: \"availabiliry\" must be int";
			}

			if (!isCorect(avalability)) {
				return "ERROR: \"availabiliry\" must be in range \"0 - 2\"";
			}

			if (!isPostedSort) {
				serviceSays = productManager.getAvalabilityProducts(mpn,
						avalability);
				if (serviceSays == null) {
					return " ERROR  this \"mpn\" = " + mpn + " is not exist";
				} else {
					return serviceSays;
				}
			}
		}

		int pricesort = 0;
		try {
			pricesort = Integer.valueOf(pricesortText);
		} catch (NumberFormatException e) {
			return "ERROR: \"pricesort\" must be int";
		}

		if (!isCorect(pricesort)) {
			return "ERROR: \"pricesort\" must be in range \"0 - 2\"";
		}

		serviceSays = productManager.getAvailabilitySortProduct(mpn,
				avalability, pricesort);
		if (serviceSays == null) {
			return " ERROR  this \"mpn\" = " + mpn + " is not exist";
		} else {
			return serviceSays;
		}
	}

	private boolean isPostedParametr(String parametrText) {
		if (parametrText != null) {
			if (parametrText.length() > 0) {
				return true;
			}
		}
		return false;
	}

	private Boolean isCorect(int parameter) {

		if (CORECT_REQUEST.contains(parameter)) {
			return true;
		}
		return false;

	}
	
	 static {
		ArrayList<Integer> corectRequest = new ArrayList<Integer>();
		corectRequest.add(0);
		corectRequest.add(1);
		corectRequest.add(2);
		CORECT_REQUEST = Collections.unmodifiableList(corectRequest);
	}
}
