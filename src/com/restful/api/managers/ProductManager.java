package com.restful.api.managers;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restful.api.beans.Product;
import com.restful.api.beans.Shop;
import com.restful.api.beans.ShopDecreaseComparator;
import com.restful.api.services.ProductService;

@Service
public class ProductManager {

	@Autowired
	private ProductService productService;

	private boolean isInitialize = false;

	public Product getProductByMPN(String mpn) {
		return productService.getProductByMPN(mpn);

	}

	public boolean isInitialize() {
		return isInitialize;
	}

	public void setInitialize(boolean isInitialize) {
		this.isInitialize = isInitialize;
	}

	public Product getAvalabilityProducts(String mpn, int avalability) {

		if (avalability == 0) {
			return productService.getProductByMPN(mpn);
		}
		Product product = productService.getProductByMPN(mpn);
		if (product == null) {
			return null;
		}

		List<Shop> shopList = product.getArray();

		if (avalability == 1) {
			Iterator<Shop> iter = shopList.iterator();
			while (iter.hasNext()) {
				if (iter.next().getStock() == 0) {
					iter.remove();
				}
			}
			return product;
		} else {
			Iterator<Shop> iter = shopList.iterator();
			while (iter.hasNext()) {
				if (iter.next().getStock() != 2) {
					iter.remove();
				}
			}
			return product;
		}
	}

	public Product getAvailabilitySortProduct(String mpn, int avalavility,
			int pricesort) {
		Product product = getAvalabilityProducts(mpn, avalavility);
		if (product == null) {
			return null;
		}

		if (pricesort == 0) {
			return product;
		}
		List<Shop> shopList = product.getArray();

		if (shopList.size() < 2) {
			return product;
		}

		if (pricesort == 1) {
			Collections.sort(shopList);
			return product;
		} else {
			Collections.sort(shopList, new ShopDecreaseComparator());
		}

		return product;
	}

	public void initializeBD() {
		if (!isInitialize) {
			productService.initializeBD();
			setInitialize(true);
		}

	}

}
