package com.restful.api.services;

import javax.json.Json;
import javax.json.JsonObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.restful.api.beans.Product;

@Service
public class ProductService {

	@Autowired
	private StringRedisTemplate template;

	public Product getProductByMPN(String mpn) {

		Product product = new Product();
		Gson gson = new Gson();
		String stringProduct = (String) template.opsForHash().get(mpn, mpn);
		product = gson.fromJson(stringProduct, Product.class);
		return product;

	}

	public void initializeBD() {

		String key = "TT";
		String key2 = "123";
		String key3 = "1";
		String key4 = "MM";
		String key5 = "DD-10";

		JsonObject jsonProduct = Json
				.createObjectBuilder()
				.add("mpn", "TT")
				.add("id", "112211")
				.add("array",
						Json.createArrayBuilder()
								.add(Json.createObjectBuilder().add("id", "17")
										.add("price", "200.91")
										.add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "192.90")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "250.90")
										.add("stock", "2"))).build();

		String json = jsonProduct.toString();
		template.opsForHash().put(key, key, json);

		jsonProduct = Json
				.createObjectBuilder()
				.add("mpn", "123")
				.add("id", "112211")
				.add("array",
						Json.createArrayBuilder()
								.add(Json.createObjectBuilder().add("id", "17")
										.add("price", "200.91")
										.add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "192.90")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "250.90")
										.add("stock", "2"))
								.add(Json.createObjectBuilder().add("id", "20")
										.add("price", "0.91").add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "14")
										.add("price", "92.90")
										.add("stock", "2"))
								.add(Json.createObjectBuilder().add("id", "13")
										.add("price", "550.90")
										.add("stock", "2"))).build();

		json = jsonProduct.toString();
		template.opsForHash().put(key2, key2, json);

		jsonProduct = Json
				.createObjectBuilder()
				.add("mpn", "1")
				.add("id", "112211")
				.add("array",
						Json.createArrayBuilder()
								.add(Json.createObjectBuilder().add("id", "17")
										.add("price", "800.91")
										.add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "122.90")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "250.90")
										.add("stock", "2"))).build();

		json = jsonProduct.toString();
		template.opsForHash().put(key3, key3, json);

		jsonProduct = Json
				.createObjectBuilder()
				.add("mpn", "123")
				.add("id", "112211")
				.add("array",
						Json.createArrayBuilder()
								.add(Json.createObjectBuilder().add("id", "17")
										.add("price", "200.91")
										.add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "192.90")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "1250.90")
										.add("stock", "2"))
								.add(Json.createObjectBuilder().add("id", "20")
										.add("price", "0.91").add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "14")
										.add("price", "920.90")
										.add("stock", "2"))
								.add(Json.createObjectBuilder().add("id", "13")
										.add("price", "550.90")
										.add("stock", "0"))).build();

		json = jsonProduct.toString();
		template.opsForHash().put(key4, key4, json);

		jsonProduct = Json
				.createObjectBuilder()
				.add("mpn", key5)
				.add("id", "112211")
				.add("array",
						Json.createArrayBuilder()
								.add(Json.createObjectBuilder().add("id", "17")
										.add("price", "210.91")
										.add("stock", "0"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "19002.90")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "11")
										.add("price", "1.90").add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "20")
										.add("price", "10.91")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "14")
										.add("price", "920.90")
										.add("stock", "1"))
								.add(Json.createObjectBuilder().add("id", "13")
										.add("price", "550.90")
										.add("stock", "1"))).build();

		json = jsonProduct.toString();
		template.opsForHash().put(key5, key5, json);

	}

}
